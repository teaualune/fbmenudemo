//
//  CiFViewController.h
//  FBMenuDemo
//
//  Created by Teaualune Tseng on 12/11/20.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CiFMenuViewController.h"

@interface CiFViewController : UIViewController

-(IBAction)showMenu:(id)sender;

@end
