//
//  CiFMenuViewController.m
//  FBMenuDemo
//
//  Created by Teaualune Tseng on 12/11/20.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#define ROOT_VIEW_CONTROLLER [[UIApplication sharedApplication].delegate window].rootViewController

#import "CiFMenuViewController.h"
#import "CiFAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "CiFMenuDataSourceHelper.h"

static NSString *menuTableCellIdentifier = @"menuTableCellIdentifier";

@interface CiFMenuViewController (){
    UITapGestureRecognizer *tapGesture;
    UIPanGestureRecognizer *panGesture;
    NSInteger menuWidth;
    
    UIImageView *_ssImage; // screen shot image view
    UIView *_ssShadow; // screen shot shadown view
}

- (void)setContentViewController:(UIViewController *) viewController atIndex:(NSInteger) index;
- (void)prepareToShowAnotherContentViewControllerAtIdex:(NSInteger) index;
- (void)slideThenHide;
- (void)singleTapScreenShot:(UITapGestureRecognizer *)gestureRecognizer;
- (void)panGestureMoveAround:(UIPanGestureRecognizer *)gesture;
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation CiFMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _screenShotView = [[UIView alloc] init];
        _contentViewControllers = [[NSMutableDictionary alloc] initWithCapacity:[CiFMenuDataSourceHelper numberOfItems]];
        menuWidth = -1;
        _ssImage = [[UIImageView alloc] init];
        _ssShadow = [[UIView alloc] init];
    }
    return self;
}

+(void)setupAtApplicationDidFinishLaunching
{
    CiFMenuViewController *menu = [CiFMenuViewController sharedMenu];
    menu.currentViewController = ROOT_VIEW_CONTROLLER;
    [menu setContentViewController:ROOT_VIEW_CONTROLLER atIndex:0];
}

+(CiFMenuViewController *) sharedMenu
{
    static CiFMenuViewController *menu;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        menu = [[CiFMenuViewController alloc] initWithNibName:@"CiFMenuView" bundle:nil];
    });
    return menu;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view addSubview:self.screenShotView];
    [self.screenShotView addSubview:_ssShadow];
    [self.screenShotView addSubview:_ssImage];
    _ssImage.userInteractionEnabled = self.screenShotView.userInteractionEnabled = YES;
    
    [menuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:menuTableCellIdentifier];
    
    // setup screenShotView
    self.screenShotView.frame = _ssImage.frame = _ssShadow.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 20);
    CALayer *screenShowLayer = _ssImage.layer;
    screenShowLayer.backgroundColor = [UIColor clearColor].CGColor;
    screenShowLayer.cornerRadius = 4.0f;
    screenShowLayer.masksToBounds = YES;
    
    CALayer *shadowLayer = _ssShadow.layer;
    shadowLayer.shadowOffset = CGSizeMake(-1, 0);
    shadowLayer.shadowColor = [UIColor blackColor].CGColor;
    shadowLayer.shadowOpacity = 0.5;
    CGRect frame = _ssImage.frame;
    shadowLayer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(frame.origin.x, frame.origin.y + 2.0f, frame.size.width, frame.size.height - 4.0f)].CGPath;
    
    // create a UITapGestureRecognizer to detect when the screenshot recieves a single tap
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapScreenShot:)];
    [self.screenShotView addGestureRecognizer:tapGesture];
    
    // create a UIPanGestureRecognizer to detect when the screenshot is touched and dragged
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureMoveAround:)];
    [panGesture setMaximumNumberOfTouches:2];
    [panGesture setDelegate:self];
    [self.screenShotView addGestureRecognizer:panGesture];
}

- (void)viewWillAppear:(BOOL)animated
{
    CGRect frame = _ssImage.frame;
    
    NSInteger pullOutWidth = (menuWidth < 0) ? MENU_WIDTH : frame.size.width - menuWidth;
    
    // now we'll animate it across to the right over 0.2 seconds with an Ease In and Out curve
    // this uses blocks to do the animation. Inside the block the frame of the UIImageView has its
    // x value changed to where it will end up with the animation is complete.
    // this animation doesn't require any action when completed so the block is left empty
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self.screenShotView setFrame:CGRectMake(pullOutWidth, frame.origin.y, frame.size.width, frame.size.height)];
        
    } completion:^(BOOL finished){
        menuWidth = -1;
    }];
}

-(void)dealloc
{
    // remove the gesture recognizers
    [self.screenShotView removeGestureRecognizer:tapGesture];
    [self.screenShotView removeGestureRecognizer:panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAnotherContentViewControllerAtIndex:(NSInteger) index animated:(BOOL) animate
{
    NSString *key = [NSString stringWithFormat:@"%d", index];
    if (self.contentViewControllers[key] != nil) {
        UIViewController *vc = self.contentViewControllers[key];
        self.currentViewController = vc;
        if (animate && ROOT_VIEW_CONTROLLER == self) [self slideThenHide];
        else [self hideMenu];
    }
}

-(void) showMenu
{
    // before swaping the views, we'll take a "screenshot" of the current view
    // by rendering its CALayer into the an ImageContext then saving that off to a UIImage
    CGSize viewSize = self.currentViewController.view.bounds.size;
    UIGraphicsBeginImageContextWithOptions(viewSize, NO, 0.0);// [UIScreen mainScreen].scale);
    [self.currentViewController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // Read the UIImage object
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // pass this image off to the MenuViewController then swap it in as the rootViewController
    _ssImage.image = image;
    
    ROOT_VIEW_CONTROLLER = self;
}

- (void)showMenu:(id)sender
{
    //UIView *view = (UIView *) sender;
    if ([sender respondsToSelector:@selector(frame)])
        menuWidth = (NSInteger) [sender frame].size.width;
    else if ([sender isKindOfClass:[UIBarButtonItem class]]) {
        UIBarButtonItem *item = (UIBarButtonItem *) sender;
        UIView *view = [item valueForKey:@"view"];
        menuWidth = view ? (NSInteger) ([view frame].size.width+2*[view frame].origin.x) : (NSInteger) 0;
    }
    
    [self showMenu];
}

#pragma mark - private methods

-(void)hideMenu
{
    ROOT_VIEW_CONTROLLER = self.currentViewController;
}

-(void) setContentViewController:(UIViewController *) viewController atIndex:(NSInteger) index
{
    [((NSMutableDictionary *) _contentViewControllers) setObject:viewController forKey: [NSString stringWithFormat:@"%d", index]];
}

- (void)prepareToShowAnotherContentViewControllerAtIdex:(NSInteger) index
{
    NSNumber *key = [NSString stringWithFormat:@"%d", index];
    if (index >= [self.contentViewControllers count] || self.contentViewControllers[key] == nil) {
        UIViewController *vc = [[UIStoryboard storyboardWithName:[CiFMenuDataSourceHelper
                                                                  storyboardNameForIndex:index]
                                                          bundle:nil]
                                instantiateViewControllerWithIdentifier:[CiFMenuDataSourceHelper
                                                                         storyboardIdentifierForIndex:index]];
        [self setContentViewController:vc atIndex:index];
    }
    [self showAnotherContentViewControllerAtIndex:index animated:YES];
}

-(void) slideThenHide
{
    // this animates the screenshot back to the left before telling the app delegate to swap out the MenuViewController
    // it tells the app delegate using the completion block of the animation
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect frame = _ssImage.frame;
        frame.origin.x = frame.origin.y = 0;
        [self.screenShotView setFrame:frame];
    } completion:^(BOOL finished){
        [self hideMenu];
    }];
}

#pragma mark - gesture related

- (void)singleTapScreenShot:(UITapGestureRecognizer *)gestureRecognizer
{
    // on a single tap of the screenshot, assume the user is done viewing the menu
    // and call the slideThenHide function
    [self slideThenHide];
}

-(void)panGestureMoveAround:(UIPanGestureRecognizer *)gesture
{
    UIView *piece = [gesture view];
    [self adjustAnchorPointForGestureRecognizer:gesture];
    
    if ([gesture state] == UIGestureRecognizerStateBegan || [gesture state] == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [gesture translationInView:[piece superview]];
        
        // I edited this line so that the image view cannont move vertically
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y)];
        [gesture setTranslation:CGPointZero inView:[piece superview]];
    }
    else if ([gesture state] == UIGestureRecognizerStateEnded)
        [self slideThenHide];
}

- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}

#pragma mark - table view delegate and datasource

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    [self prepareToShowAnotherContentViewControllerAtIdex:index];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuTableCellIdentifier];
    
    NSInteger index = indexPath.row;
    
    cell.textLabel.text = [CiFMenuDataSourceHelper cellNameForIndex:index];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [CiFMenuDataSourceHelper numberOfItems];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

@end
