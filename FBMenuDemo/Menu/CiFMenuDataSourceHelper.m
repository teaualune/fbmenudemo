//
//  CiFMenuDataSourceHelper.m
//  FBMenuDemo
//
//  Created by Teaualune Tseng on 12/11/20.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//
//  1. In storyboardNameForIndex:, add the name of storyboard in the corresponding index
//  2. In storyboardIdentifierForIndex:, add the identifier of view in the corresponding index
//  3. In numberOfItems, change the return value to the number of table entries
//
//  Example:
//  Add a new storyboard called "CheckinStoryboard.storyboard" and a view with identifier "ShakeViewController";
//  the shake view wants to be placed at the second entry of the menu, so:
//  in storyboardNameForIndex:, case 1 should return "CheckinStoryboard";
//  in storyboardIdentifierForIndex:, case 1 should return "ShakeViewController"

#import "CiFMenuDataSourceHelper.h"

@implementation CiFMenuDataSourceHelper

+(NSString *) storyboardNameForIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        case 1:
        case 2:
            return @"MainStoryboard";
            break;
            
        default:
            return @"MainStoryboard";
            break;
    }
}

+(NSString *) storyboardIdentifierForIndex:(NSInteger)index
{
    
    if (index == 0) {
        return @"view1";
    } else if (index == 1) {
        return @"view2";
    } else if (index == 2) {
        return @"view3";
    } else {
        return @"";
    }
}

+(NSString *) cellNameForIndex: (NSInteger)index
{
    if (index == 0) {
        return @"View1";
    } else if (index == 1) {
        return @"View2";
    } else if (index == 2) {
        return @"View3";
    } else {
        return @"??";
    }
}

+(NSInteger) numberOfItems
{
    return 3;
}

@end
