//
//  CiFMenuDataSourceHelper.h
//  FBMenuDemo
//
//  Created by Teaualune Tseng on 12/11/20.
//  Copyright (c) 2012年 Teaualune Tseng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CiFMenuDataSourceHelper : NSObject

+(NSString *) storyboardNameForIndex:(NSInteger)index;
+(NSString *) storyboardIdentifierForIndex:(NSInteger)index;
+(NSString *) cellNameForIndex: (NSInteger)index;
+(NSInteger) numberOfItems;

@end
