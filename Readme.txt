Step 1: set up CiFMenuViewController in your project

(CiFMenuViewController.h)
1. Add five files to project
2. Include CiFMenuViewController.h file at app delegate and run setupAtApplicationDidFinishLaunching at applicationDidFinishLaunching: withOption:
3. In any views that require to show menu, call [[CiFMenuViewController sharedMenu] showMenu]
(Or [[CiFMenuViewController sharedMenu] showMenu:sender], if you want to set menu width corresponding to button width)
4. Go to CiFMenuDataSourceHelper.m to edit content views.


Step 2: edit menu

(CiFMenuDataSourceHelper.m)
1. In storyboardNameForIndex:, add the name of storyboard in the corresponding index
2. In storyboardIdentifierForIndex:, add the identifier of view in the corresponding index
3. In cellNameForIndex:, add the cell title in the corresponding index
4. In numberOfItems, change the return value to the number of table entries

Example:
Add a new storyboard called "CheckinStoryboard.storyboard" and a view with identifier "ShakeViewController";
the shake view wants to be placed at the second entry of the menu, so:
in storyboardNameForIndex:, case 1 should return "CheckinStoryboard";
in storyboardIdentifierForIndex:, case 1 should return "ShakeViewController"
